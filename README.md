# Camunda NodeJS External Task Client

1. Running Camunda
   First, make sure to have Camunda running.

2. Deploying the Workflow
   Download the following model
   and deploy it using the Camunda Modeler.

3. Installing the NPM package
   Install camunda-external-task-client-js from npm using:

```npm install -s camunda-external-task-client-js```

4. Consuming Service Tasks in NodeJS

```
const { Client, logger, BasicAuthInterceptor } = require("camunda-external-task-client-js");

const basicAuthentication = new BasicAuthInterceptor({
    username: "demo",
    password: "demo"
});

// configuration for the Client:
//  - 'baseUrl': url to the Workflow Engine
//  - 'logger': utility to automatically log important events
//  - 'interceptors': Basic Authentication
const config = { baseUrl: "http://localhost:30091/domain-process-engine/engine-rest", use: logger, interceptors: basicAuthentication };

// create a Client instance with custom configuration
const client = new Client(config);

// subscribe to the topic: 'domain-billing'
client.subscribe("domain.billing", ({ task, taskService }) => {
    // Put your business logic
    // complete the task
    taskService.complete(task);
});

client.subscribe("domain.delivery", ({ task, taskService }) => {
    // Put your business logic
    // complete the task
    taskService.complete(task);
});

client.subscribe("domain.notification", ({ task, taskService }) => {
    // Put your business logic
    // complete the task
    taskService.complete(task);
});
```
The output should be:
```
polling
✓ subscribed to topic domain-billing
✓ subscribed to topic domain-delivery
✓ subscribed to topic domain-notification
polling
✓ polled 10 tasks
✓ completed task 897ce191-2dea-11e8-a9c0-66b11439c29a
```
