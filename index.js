const { Client, logger, BasicAuthInterceptor } = require("camunda-external-task-client-js");

const basicAuthentication = new BasicAuthInterceptor({
    username: "demo",
    password: "demo"
});

// configuration for the Client:
//  - 'baseUrl': url to the Workflow Engine
//  - 'logger': utility to automatically log important events
//  - 'interceptors': Basic Authentication
const config = { baseUrl: "http://localhost:30091/domain-process-engine/engine-rest", use: logger, interceptors: basicAuthentication };

// create a Client instance with custom configuration
const client = new Client(config);

// subscribe to the topic: 'domain-billing'
client.subscribe("domain.billing", ({ task, taskService }) => {
    // Put your business logic
    // complete the task
    taskService.complete(task);
});

client.subscribe("domain.delivery", ({ task, taskService }) => {
    // Put your business logic
    // complete the task
    taskService.complete(task);
});

client.subscribe("domain.notification", ({ task, taskService }) => {
    // Put your business logic
    // complete the task
    taskService.complete(task);
});